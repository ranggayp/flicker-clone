import React, { useEffect, useState } from "react";
import axios from "axios";

export default function App() {
  const [photos, setPhotos] = useState([]);
  const [inputUser, setInputUser] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    axios
      .get(
        "https://www.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=79f13fb9d1347c566b5dcb82d347859b&format=json&nojsoncallback=true"
      )
      .then((e) => {
        setPhotos(e.data.photos.photo);
      });
  }, []);

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const current = photos.slice(indexOfFirst, indexOfLast);

  const renderPhotos = current.map((e, i) => {
    return (
      <img
        key={i}
        src={`https://live.staticflickr.com/${e.server}/${e.id}_${e.secret}.jpg`}
      ></img>
    );
  });

  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(photos.length / perPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map((number) => {
    return (
      <span
        key={number}
        id={number}
        onClick={(e) => setCurrentPage(Number(e.target.id))}
        style={{
          cursor: "pointer",
          fontWeight: number === currentPage ? "bold" : "normal",
          color: number === currentPage ? "#20cb9d" : "#000",
        }}
      >
        {number} &nbsp;
      </span>
    );
  });

  if (loading) {
    return <h1>Please wait, Loading</h1>;
  }

  return (
    <div>
      <h1>Flicker</h1>
      <input onChange={(e) => setInputUser(e.target.value)} />
      <button
        onClick={async () => {
          setLoading(true);
          await axios
            .get(
              `https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=79f13fb9d1347c566b5dcb82d347859b&tags=${inputUser}&format=json&nojsoncallback=true`
            )
            .then((e) => {
              setPhotos(e.data.photos.photo);
              setLoading(false);
            });
        }}
      >
        Submit
      </button>
      <div>{renderPhotos}</div>
      <div>{renderPageNumbers}</div>
    </div>
  );
}
